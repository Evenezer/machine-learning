# Machine Learning Algorithms for Breast Cancer Identification

#Imports
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import cufflinks as cf
import numpy as np
import plotly.express as px

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV

from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import StratifiedKFold

from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from imblearn.under_sampling import RandomUnderSampler, TomekLinks
from imblearn.over_sampling import RandomOverSampler, SMOTENC
from sklearn.impute import SimpleImputer

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

# Set matplotlib in Darkmode
plt.style.use('dark_background')


# Load the Dataset

def load_dataset(path:str) -> pd:
    return pd.read_csv(path)
# Loading data using pandas
path = 'Breast Cancer.csv'
dataset = load_dataset(path)

# Explore the Dataset
# Explore the dataset
dataset.info()
# Check if there are missing values
# This shows if any columns have NA or NAN
dataset.isna().any()

#Plotting data
def plot_data(dataset):
    """Plot out all the data from the dataset in different histograms"""
    dataset.hist(bins=50, figsize = (20,15))
    plt.show()
    return

# Remove sample code number because that is not a feature
dataset = dataset.drop('Sample code number', axis=1)
plot_data(dataset)

# 3. Correlation Matrix

def correlation_heatmap(corr):
    """Plot feature correlation heatmap"""
    f, ax = plt.subplots(figsize=(10, 8))
    sns.heatmap(corr, mask=np.zeros_like(corr, dtype=np.bool), annot = True,
                cmap=sns.diverging_palette(220, 10, as_cmap=True),
                square=True, ax=ax)
    return
# Correlation data
correlation_output = dataset.corr()

# Correlation matrix
correlation_heatmap(correlation_output)

# Try Feature Engineering

dataset['UCSh/UCSi Ratio'] = dataset['Uniformity of Cell Shape'] / np.sqrt(dataset['Uniformity of Cell Size'])
correlation_output_2 = dataset.drop(['Uniformity of Cell Shape', 'Uniformity of Cell Size'], axis=1).corr()

# Correlation data if we drop some rows
# correlation_output_2 = dataset.drop('Uniformity of Cell Shape', axis=1).corr()

# Correlation matrix
correlation_heatmap(correlation_output_2)

#There are several ways to decrease the strong correlation between the two features. Dropping one of the highly correlated features is a possibility, but it is not optimal because valuable information may be lost since there are only a few features. Alternatively, feature engineering can be used. In this particular case, a new feature called 'UCSh/UCSi Ratio' was created by dividing UCSh by the square root of UCSi. Next, the correlated features were removed, resulting in an improved correlation matrix

dataset = dataset.drop(['Uniformity of Cell Shape', 'Uniformity of Cell Size'], axis=1)


# Functions for checking Parameters
from sklearn.metrics import roc_auc_score, roc_curve, auc


# Check AOC ARC curve
def aoc_arc_curve(pred, labels_test):
    """Plots the AOC ARC curve and score"""
    auc_score = roc_auc_score(labels_test, pred)
    print('AUROC score: {:.2f}'.format(auc_score))

    false_positive_rate, true_positive_rate, thresholds = roc_curve(labels_test, pred, pos_label=4)

    plt.figure(figsize=(5, 4), dpi=100)
    plt.axis('scaled')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.title("AUC & ROC Curve")
    plt.plot(false_positive_rate, true_positive_rate, 'g')

    plt.plot([0, 1], [0, 1], color='blue', linestyle='dotted')

    plt.fill_between(false_positive_rate, true_positive_rate, facecolor='lightgreen', alpha=0.7)
    plt.text(0.95, 0.05, 'AUC = {:.2f}'.format(auc_score), ha='right', fontsize=12, weight='bold')
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.show()

    return auc_score


def parameter_check(pred, labels_test):
    """Determines accuracy, precision, recall, f1 and aoc arc curve"""
    # Check accuracy
    accuracy = accuracy_score(labels_test, pred)

    # Check precision
    precision = precision_score(labels_test, pred, pos_label=4)
    # Check recall
    recall = recall_score(labels_test, pred, pos_label=4)
    # Check F1 score
    F1 = f1_score(labels_test, pred, pos_label=4)
    print('Accuracy: {:.2f}'.format(accuracy))
    print('Precision: {:.2f}'.format(precision))
    print('Recall: {:.2f}'.format(recall))
    print('F1 score: {:.2f}'.format(F1))

    auc = aoc_arc_curve(pred, labels_test)

    return {'accuracy': accuracy, 'precision': precision, 'recall': recall, 'fi': F1, 'auc': auc}



#According to the literature [1], the relevant features are:

 #Uniformity of cell size
 #Single epithelial cell size
 #Bland chromatin
 #Normal nucleoli


 # Data Pre-Processing
from sklearn.model_selection import train_test_split

# Create features and labels
features = dataset.drop(['Class'], axis=1)

labels = dataset['Class']

# Create training (75%) and test (25%) sets
features_train, features_test, labels_train, labels_test = train_test_split(features, labels,
                                                                            random_state=42, test_size =0.25)

# Feature Selection

# Future importance Plot
def feature_importance(classifier, features_train, labels_train):
    """Plot feature importance"""
    classifier.fit(features_train, labels_train)

    # Creating a new data frame
    feature_importance = pd.DataFrame()
    feature_importance['Features'] =  list(features_train.columns)
    feature_importance['Importance'] = list(classifier.coef_[0]) # values in dataframe
    feature_importance.sort_values(by=['Importance'], ascending=True, inplace=True)
    feature_importance['positive'] = feature_importance['Importance'] > 0 # ranking of feat.
    feature_importance.set_index('Features', inplace=True) # Set the DataFrame index

        # Creating plot of feature importance
    feature_importance.Importance.plot(kind='barh', figsize=(11, 6),color =
        feature_importance.positive.map({True: 'blue', False: 'red'}))

    plt.xlabel('Importance')
    plt.ylabel('Features')
    plt.title('Feature contribution to breast cancer')
    plt.show()


### Classifier

def run_classifier(classifier, features_train, labels_train, features_test, labels_test) -> dict:
    # Fit data for training into classifier
    classifier.fit(features_train , labels_train)
    # Predicting with classifier
    pred = classifier.predict(features_test)
    return parameter_check(pred, labels_test)


#### Pipline

def build_pipeline(params, classifier, rskf=False):
    # Setting up a pipeline
    pipe = Pipeline([
        ('scaler', None),
        ('imputer', None),
        ('balancing', None),
        ('clf', classifier)
    ])

    # Cross validation
    if rskf:
        cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=3, random_state=42)
    else:
        cv = StratifiedKFold(n_splits=5,random_state=42,shuffle=True)

    # Set up the grid search
    gs = GridSearchCV(pipe,params,n_jobs=-1,cv=cv,scoring='accuracy')

    # Fit gs to training data
    gs_results = gs.fit(features_train, labels_train)

    # Find best fit
    print(f'Best score: {gs_results.best_score_}')
    print(f'Best parameters: \n{gs_results.best_params_}')

    # Get score
    print(f'\nScore: {gs.score(features_test, labels_test)}')

    # Predict results
    labels_preds = gs.predict(features_test)
    best_par = parameter_check(labels_preds, labels_test)

    return best_par, gs_results



#### KNN Classifier - Pipeline
# Create KNN Classifier
classifier_knn = KNeighborsClassifier()

# Parameter grid to search over using grid search
params={
    'imputer': [SimpleImputer(missing_values=0)],
    'imputer__strategy': ['mean', 'median'],
    'balancing': ['passthrough',
                  RandomOverSampler(random_state=42),
                  SMOTENC(categorical_features=[0], random_state=42, k_neighbors=0),
                  RandomUnderSampler(random_state=42),
                  TomekLinks()
                  ],
    'scaler':[StandardScaler(), MinMaxScaler()],
    'clf__n_neighbors':[3,5,7,9,11,13],
    'clf__weights':['uniform', 'distance'],
    'clf__metric': ['minkowski', 'manhattan', 'euclidian']
}

best_par_knn, score_knn = build_pipeline(params,classifier_knn)

#### SVM Classfier - Pipline

from sklearn.svm import SVC

# Create SVM Classifier
classifier_svm = SVC()

# Parameter grid to search over using grid search
params={
    'imputer': [SimpleImputer(missing_values=0)],
    'imputer__strategy': ['mean', 'median'],
    'balancing': ['passthrough',
                  RandomOverSampler(random_state=42),
                  SMOTENC(categorical_features=[0], random_state=42, k_neighbors=0),
                  RandomUnderSampler(random_state=42),
                  TomekLinks()
                  ],
    'scaler':[StandardScaler(), MinMaxScaler()],
    'clf__degree':[0.001, 0.01, 0.1, 1, 10],
    'clf__gamma' : [1,2,3,4,5]
    #'clf__kernel' :['rbf', 'linear', 'poly' ,'sigmoid']
}

best_par_svm, score_svm = build_pipeline(params, classifier_svm)


feature_importance(SVC(kernel='linear'), features_train, labels_train)




#### Logistic Regression - Pipline

from sklearn.linear_model import LogisticRegression

# Create Logistic Regression Classifier
classifier_lr = LogisticRegression()

# Parameter grid to search over using grid search
params={
    'imputer': [SimpleImputer(missing_values=0)],
    'imputer__strategy': ['mean', 'median'],
    'balancing': ['passthrough',
                  RandomOverSampler(random_state=42),
                  SMOTENC(categorical_features=[0], random_state=42, k_neighbors=0),
                  RandomUnderSampler(random_state=42),
                  TomekLinks()
                  ],
    'scaler':[StandardScaler(), MinMaxScaler()],
    'clf__solver':['newton-cg', 'lbfgs', 'liblinear', 'sag'],
    'clf__multi_class':['ovr', 'multinomial'],
    'clf__class_weight':['None', 'balanced']
}
best_par_lr, score_lr = build_pipeline(params, classifier_lr)
feature_importance(classifier_lr, features_train, labels_train)

#### Random Forest Classifier - Pipline

from sklearn.ensemble import RandomForestClassifier

# Create Random Forest Classifier
classifier_rf = RandomForestClassifier(criterion='entropy')

# Parameter grid to search over using grid search
params={
    'clf__n_estimators':[10, 50, 100, 500],
    'clf__max_depth': [20,30,50],
    'clf__min_samples_split': [2,3,4],
    'clf__min_samples_leaf': [3,5]
}

best_par_rf, score_rf = build_pipeline(params, classifier_rf)

feature_importance(LogisticRegression(), features_train, labels_train)


### AdaBoost Classifier - Pipline
from sklearn.ensemble import AdaBoostClassifier

# Create AdaBoost Classifier
classifier_ada = AdaBoostClassifier()

# Parameter grid to search over using grid search
params={
    'clf__n_estimators':[10, 50, 100, 500],
    'clf__learning_rate': [0.0001, 0.001, 0.01, 0.1, 1.0]
}
best_par_ada, score_ada = build_pipeline(params, classifier_ada, rskf=True)


#### GradientBosst Classifier - Pipeline

from sklearn.ensemble import GradientBoostingClassifier

# Create GradientBoost Classifier
classifier_grad = GradientBoostingClassifier()

# Parameter grid to search over using grid search
params={
    'clf__n_estimators':[5,50,250,500],
    'clf__max_depth' : [1,3,5,7,9],
    'clf__learning_rate': [0.01,0.1,1,10,100],
    'clf__min_samples_split': [2,3,4],
    'clf__min_samples_leaf': [3,5]

}

best_par_grad, score_grad = build_pipeline(params, classifier_grad, rskf=True)


## Algorithm Comparison: Parameters
# Plot out different parameters
compare = [best_par_knn, best_par_svm, best_par_lr, best_par_rf, best_par_ada, best_par_grad ]
headers_comparison = [ 'KNN', 'SVM','Logistic Regression','RandomForest','AdaBoost', 'GradBoost']
compare_df = pd.DataFrame(compare)
compare_df["results"]=headers_comparison
compare_df = compare_df.set_index('results', drop=True).rename_axis(None)
compare_df



"""To summarize, the breast cancer dataset was analyzed using various machine learning (ML) algorithms to determine which one performs the best. The RandomForestClassifier algorithm had the highest AUR ROC score. The feature importances were compared to relevant literature, and it was found that the UCSh/UCSi Ratio (an engineered feature) and Bland Chromatin were the two most relevant features for the RandomForestClassifier, which aligned with three out of four of the most relevant features in literature.

Before running the chosen algorithm (RandomForestClassifier), the data required scaling but not balancing. There were initially two highly correlated features, which were mitigated through feature engineering. The study referred to in this analysis is "Feature selection using genetic algorithm for breast cancer diagnosis: experiment on three different datasets" by Aalaei et al. (2016). The reference can be found at https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4923467/."""