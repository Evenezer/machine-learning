# Machine Learning Algorithms for Breast Cancer Identification
## This repository contains code for identifying breast cancer using machine learning algorithms. The goal is to build a model that can predict whether a tumor is benign or malignant.

## Requirements
This code requires the following packages:

pandas
seaborn
matplotlib
cufflinks
numpy
plotly
scikit-learn
imblearn
Data
The data used in this project can be found in the Breast Cancer.csv file. It contains information about various features of breast tumors and whether they are benign or malignant.

## Usage
To use this code, simply clone the repository and run the breast_cancer_identification.ipynb notebook in Jupyter.

## Approach
The following approach was used in this project:

## Load the dataset using pandas.
Explore the dataset and visualize the data using seaborn and matplotlib.
Create a correlation heatmap using seaborn to identify highly correlated features.
Use feature engineering to create a new feature and remove highly correlated features.
Train machine learning models using scikit-learn and evaluate their performance.
Use resampling techniques from imblearn to handle class imbalance.
Algorithms
The following machine learning algorithms were used in this project:

## K-Nearest Neighbors (KNN)
## Evaluation Metrics
The following evaluation metrics were used in this project:

### Accuracy
### Precision
### Recall
### F1 Score
### Area Under ROC Curve (AUC)
### References
[1] William H. Wolberg and O.L. Mangasarian: "Multisurface method of pattern separation for medical diagnosis applied to breast cytology", Proceedings of the National Academy of Sciences, U.S.A., Volume 87, December 1990, pp 9193-9196.



SVM (Support Vector Machine):
SVM is a supervised machine learning algorithm that can be used for classification or
regression analysis. It works by creating a hyperplane that separates the data into 
different classes. The goal of SVM is to find the hyperplane that maximizes the margin 
between the classes, meaning the distance between the hyperplane and the closest data 
points from each class. SVM can handle both linear and non-linear data using different 
kernel functions.

KNN (K-Nearest Neighbors):
KNN is a supervised machine learning algorithm used for classification and regression 
analysis. It works by finding the k nearest neighbors of a given data point and 
classifying it based on the most frequent class of its neighbors. 
The choice of k can impact the model's performance, as a small k may lead to
overfitting and a large k may lead to underfitting. KNN is a non-parametric 
algorithm, meaning it does not make any assumptions about the underlying 
distribution of the data.

Random Forest:
Random Forest is a supervised machine learning algorithm that can be used for
classification and regression analysis. It works by creating multiple decision 
trees and combining their predictions. Each decision tree is trained on a random 
subset of the data and a random subset of the features, which helps to reduce overfitting.
The final prediction of the model is based on the average or majority vote of the 
predictions of the individual trees.

Gradient Boost:
Gradient Boost is a supervised machine learning algorithm used for classification
and regression analysis. It works by iteratively training weak learners 
(usually decision trees) on the residuals of the previous model, and combining
their predictions to improve the overall prediction. The key idea behind Gradient
Boost is to minimize the loss function by adjusting the model's parameters in the 
direction of the negative gradient.

Adaboost:
Adaboost (Adaptive Boosting) is a supervised machine learning algorithm used 
for classification and regression analysis. It works by creating a series of weak 
learners (usually decision trees) and combining their predictions to form a final
prediction. Each weak learner is trained on a modified version of the data, where 
the misclassified data points are given higher weights. The weights are adjusted 
after each iteration to focus on the misclassified data points, which helps to 
improve the overall performance of the model.

Logistic Regression:
Logistic Regression is a supervised machine learning algorithm used for binary 
classification problems. It works by modeling the relationship between the 
independent variables and the probability of the dependent variable 
(i.e., the binary outcome) using a logistic function. The logistic 
function maps any input to a value between 0 and 1, which represents 
the probability of the positive outcome. Logistic Regression is a linear
model, meaning it assumes a linear relationship between the independent 
variables and the log-odds of the outcome.

Cross Validation:
Cross Validation is a technique used to evaluate the performance of a
machine learning model on an independent dataset. It works by dividing the 
dataset into k equal-sized folds and using k-1 folds for training the model 
and the remaining fold for testing the model. This process is repeated k 
times, with each fold serving as the test set once. The performance of 
the model is evaluated by averaging the results of the k iterations. 
Cross Validation can help to reduce the risk of overfitting and provide 
a more accurate estimate of the model's performance